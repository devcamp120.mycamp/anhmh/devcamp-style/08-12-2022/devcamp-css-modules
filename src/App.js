import './App.css';

function App() {
  return (
    <div className="App">
      <div className="devcamp-wrapper">
        <img
          className="devcamp-avatar"
          src="https://randomuser.me/api/portraits/women/48.jpg"
          alt="Tammy Stevens"
        />
        <div className="devcamp-quote">
          <p>
            This is one of the best developer blogs on the planet! I read it daily to improve my skills.
          </p>
        </div>
        <p className="devcamp-name">
          Tammy Stevens<span> . Front End Developer</span>
        </p>
      </div>
    </div>
  );
}

export default App;
